// custom typefaces
import "typeface-montserrat"
import "typeface-raleway"
// normalize CSS across browsers
import "./src/normalize.css"
// custom CSS styles
import "./src/style.css"