---
title: Burger de poulet frit croustillant de Michel Dumas
date: "2020-12-20T13:36:00.000Z"
description: "Un burger de poulet mariné puis frit croustillant et juteux"
---

## Ingrédients

Filets de poulets sans peau

Pains à burger

Salade

Cornichons

### Pour la marinade

3 citrons

Piment frais

2 gousses d'ail

Huile d'olive

### Pour la sauce

1 oeuf

Moutarde de Dijon

Reste de marinade

Huile d'olive

### Pour la panure

Reste de marinade

4 oeufs

Farine

## Préparation

### Marinade

Laver puis presser les citrons

Mettre le jus dans un récipient 

Saler, poivrer

Réserver

Emincer le piment et l'ajouter au jus de citron

Eplucher et concasser l'ail, l'ajouter à la préparation

Mixer au mixeur plongeant

Ajouter l'huile d'olive tout en mixant jusqu'à obtenir la consistence désirée (~15cl)

### Poulet

Détailler les filets de poulet en escalopes d'environ 1 ou 2 cm d'épaisseur (couper en biais, puis en papillon pour la dernière)

Placer dans un plat large

Recouvrir de marinade, en laisser un peu pour la sauce

Tourner et retourner les escalopes pour bien les recouvrir

Couvrir le plat et réserver au réfrigérateur

Laisser mariner 3 ou 4h

### Sauce

Casser l'oeuf et le mettre dans un récipient

Ajouter une bonne cuillère de moutarde de Dijon

Saler, poivrer

Mixer à l'aide d'un mixeur plongeant

Ajouter un peu de restant de marinade tout en mixant pour monter la sauce

Ajouter de l'huile d'olive tout en mixant jusqu'à obtenir une consistence de trempette

Réserver au réfrigérateur

### Panure et friture

Sortir le poulet du réfrigérateur et le placer dans un grand bol

Verser le fond de marinade dans un récipient

Ajouter les 4 oeufs entiers

Mixer à l'aide d'un mixeur plongeant

Verser dans un plat large

Préchauffer l'huile à 195°C

Tremper le poulet dans la farine, puis les oeufs, puis la farine, plusieurs fois

Faire frire environ 1 minute

Contrôler la cuisson : le poulet doit être juteux mais cuit 

### Burger

Faire griller les pains

Tartiner avec la sauce

Monter le burger : placer une feuille de salade, le poulet, des cornichons, puis fermer avec le pain

Accompagner de frites fraiches et du restant de sauce